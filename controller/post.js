const { successResponse } = require("../helper/response")

class Post {
	constructor(){
		this.post=[
			{
				'text':'Postingan Pertama Saya',
				'created_at':'2022-18-6',
				'user_name':'jono'
			},
			{
				'text':'Postingan Kedua Saya',
				'created_at':'2022-18-6',
				'username':'shin'
			}
		]

	}
	getPost=(req,res)=>{
		successResponse(
			res,
			200,
			this.post,
			{total:this.post.length}
		)
		// res.status(200).json({
		// 	data: this.post,
		// 	meta :{
		// 		code:200,
		// 		message:{
		// 			code:200,
		// 			message:'data berhasil dikirim',
		// 			total: this.post.length
		// 		}
		// 	}
		// }
		// )
	}
	getDetailPost = (req,res) => {
		const index=req.params.index
		successResponse(
			res,
			200,
			this.post[index]

			// {total: this.post.length}
			// 'sukses ambil data'
		)
		// res.status(200).json({
		// 	data: this.post[index],
		// 	meta :{
		// 		code: 200,
		// 		message:{
		// 			code:200,
		// 			message:'sukses ambil data'
		// 			// total: this.post.length
		// 		}
		// 	}
		// }
		// )
	}
	insertPost =(req,res)=>{
		const body= req.body
		const param={
			'text':body.text,
			'created_at':new Date(),
			'username':body.username
		}
		this.post.push(param)
		successResponse(
			res,
			201,
			param
		)
	}

	// 	res.status(201).json({
	// 		data:param,
	// 		message:{
	// 			code:201,
	// 			message:'sikses ambil data banyak',

	// 		}
	// 	})
	// }
	updatePost =(req,res)=>{
	const index=req.params.index
	const body= req.body

	this.post[index].text=body.text
	this.post[index].username=body.username
	this.post[index].created_at=body.created_at

	successResponse(res,200,this.post[index])
	res.status(200).json({
		data:this.post[index],
		meta:{
			code:200,
			message:'sukses ubah data '

		}
	})

	}
	deletePost=(req,res)=>{
		const index=req.params.index

		this.post.splice(index,1)
		successResponse(res,200,null)
		// res.status(200).json({
		// 	data:null,
		// 	meta:{
		// 		code:200,
		// 		message:'sukses menghapus data '

		// 	}
		// })
	// }
	}
}
module.exports=Post